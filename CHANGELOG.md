<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

Automatically update your ninja build dependencies when files change in your
source tree.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/ninjasub/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.3 - 2023-10-23](https://gitlab.com/IanTwenty/ninjasub/-/releases/v0.0.3)

### Changed

* Allow directives to include directories by switching to find's `path` option.
  So a directive like `./src/*.lua` will work.

### Fixed

* Shellcheck warning about unreachable code on line 97 of ninjasub.

## [0.0.2 - 2023-06-23](https://gitlab.com/IanTwenty/ninjasub/-/releases/v0.0.2)

### Fixed

* Daemons now have their own log file each, logging to
  `$TMPDIR/ninjasub/PID_XXX` rather than all logging to `/tmp/something`.

## [0.0.1 - 2023-06-23](https://gitlab.com/IanTwenty/ninjasub/-/releases/v0.0.1)

* First version
