<!--
SPDX-FileCopyrightText: 2023 IanTwenty <https://gitlab.com/IanTwenty>

SPDX-License-Identifier: CC-BY-SA-4.0

Automatically update your ninja build dependencies when files change in your
source tree.

For full copyright information see the AUTHORS file at the top-level
directory of this distribution or at
[AUTHORS](https://gitlab.com/IanTwenty/ninjasub/AUTHORS.md)

This work is licensed under the Creative Commons Attribution 4.0 International
License. You should have received a copy of the license along with this work.
If not, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
-->

# Ninjasub

Automatically update your [ninja](https://ninja-build.org/) build dependencies
when files change in your source tree.

**Note** this software is in early development and caution should be taken with
its use. It has some limitations.

## Table of Contents

<!-- vim-markdown-toc GitLab -->

* [Background](#background)
* [Installation](#installation)
  * [Prerequisites](#prerequisites)
  * [Install](#install)
  * [Via bin](#via-bin)
  * [Via git clone](#via-git-clone)
* [Usage](#usage)
  * [One-off configuration of your build file](#one-off-configuration-of-your-build-file)
  * [Use ninjasub in your build file](#use-ninjasub-in-your-build-file)
* [How it works](#how-it-works)
* [To Do](#to-do)
* [Test cases](#test-cases)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)

<!-- vim-markdown-toc -->

## Background

I like [ninja](https://ninja-build.org/) - for small projects you can handcraft
a lean fast build file. But maintaining a build file by hand is a pain if your
source tree changes frequently. I just needed an extra tool that tells ninja
when my build file needs updating and manages lists of file dependencies
automatically.

## Installation

### Prerequisites

* `inotifywait`, usually part of the `inotify-tools` package or similar.

### Install

There are two ways:

### Via bin

**RECOMMENDED** Install with [bin](https://github.com/marcosnils/bin):

```bash
bin install https://gitlab.com/IanTwenty/ninjasub
```

bin will allow you to manage the install and update in future.

### Via git clone

Git clone the repository and copy ninjasub onto your path. For example:

```bash
git clone https://gitlab.com/IanTwenty/ninjasub
cd ninjasub
cp ninjasub /usr/local/bin
```

## Usage

There are some limitations:

* `ninjasub` works under BASH/Linux only for now.
* We assume you run the build from the root of your source tree and your build
  file is `build.ninja`.
* We make certain assumptions about how your build file is structured (see
  below).
* We assume you always save your build before you run it. There's a chance
  `ninjasub` could overwrite your build file whilst you have unsaved changes in
  an editor.

### One-off configuration of your build file

To use `ninjasub` put the following snippet in your build file. You only need to
add this once and `ninjasub` will be forever enabled for this build:

```ninja
rule ninjasub
  command = ninjasub MARKER

build MARKER: phony
build build.ninja: ninjasub MARKER
```

This tells ninja to check the `MARKER` file to know whether to rebuild your
`build.ninja`. When a rebuild is necessary we run `ninjasub` via the rule of the
same name.

The file `MARKER` is an empty file used by `ninjasub` to indicate to ninja that
the build file needs rebuilding. It can be any path of your choice, it will
be created when needed when `ninjasub` runs.

### Use ninjasub in your build file

Anywhere you need to insert a list of dependencies in your build just use a
commented `ninjasub` directive before the build directive:

```ninja
rule lint
  command = mdl ${in}

# ninjasub:*.md
build lint: lint $
            .README.md $
            ./docs/CONTRIBUTING.md $
            ./docs/GETTING_STARTED.md
```

Here on line 4 we instruct `ninjasub` to list any `*.md` files in the source
tree (recursively) with the ninja comment `# ninjasub:*.md`. This means our lint
rule will always run whenever any of our markdown files changes. New or deleted
markdown files will be add/removed from the list whenever you re-run the build.

Directives are fed directly into the `find` command's `path` option. You can
use any valid shell pattern, e.g.

```ninja
# ninjasub:./src/*.h
```

At the moment we assume the build directive that follows a `ninjasub` directive
is contained in just one line. We may make this more flexible in future.

You can add as many `ninjasub` directives as you like to your build, each will
be evaluated separately.

## How it works

`Ninjasub` tries to do the minimum to monitor your source tree but keep your
build file up-to-date every time you build.

An overview of the logic:

* When you run your build the first time, the MARKER file will be missing and
  therefore ninja will invoke `ninjasub`.
* `ninjasub` will launch a daemon version of itself that:
  * Creates the MARKER file.
  * Monitors your source tree. If any changes are seen the MARKER file is
    touched.
  * After 60s of inactivity the daemon will terminate and delete the MARKER
    file.
  * Note that `ninjasub` will never launch more than one daemon for each source
    tree.
* After the daemon is launched `ninjasub` will continue and inspect your
  build file for any directives, expand those and rewrite your build file if
  necessary.
* On any subsequent build runs ninja will either:
  * See that the daemon has touched the MARKER and therefore we need to update
    the build file again, running `ninjasub` once more.
  * See that the MARKER has disappeared (perhaps the daemon has timed out) and
    rerun `ninjasub` again (a new daemon will be launched).

## To Do

* Accumulate some real-world experience with the tool, gather test cases and
  reqs.
  * For bashtic:
    * We need to say docs/* but our find cmd uses `name` which only matches
      final filename. `wholename` might make more sense as its matched against
      the start point concat with whole filename. So `./docs/*` would work
    * Respect files known to git only, i.e. use gitignore and also ignore any
      gitmodules such as bats.
    * It would be nice to shellcheck all bash/scripts etc but how to locate them
      as name is not enough. Do we look for shebangs?
    * We need to catch if the daemon fails and output stdout/stderr to user
      otherwise they have no easy way to troubleshoot.
    * We are creating our ninjasub tmp dir with permissions for the user so
      another user on the same system cannot use it and their ninjasub will
      always fail. Create with "o+rw".
    * Could `reuse lint` from galagos potentially use ninjasub? So it doesnt run
      unless necc?
    * Be nice to do a template:
      * For each bats file do a sentinal so we only run the test we need to.
        Though maybe bats will support incremental run one day?
* Introduce testing before we fix any more bugs.

## Test cases

Real-world testing:

* A build command at the end of the file with no deps (yet) does not get them
  added. Are we coming to end of build file before we write out globs perhaps?
* If you write an new build command with NO deps you have to terminate it
  without a line continuation: `build thing: cmd`. But when ninjasub adds some
  deps we need to add the continuation in or the build file will not parse.

Build file rebuild loops:

* Marker was missing when we began, but no changes needed to build file
* Marker was newer then build file, but no changes required
* Must be some more?
* Must be some race situations

Daemon mode:

* Don't launch if already existing
* Timeout
* Marker file does not exist before, exists during run, is gone at the end
* Marker file gone even on abnormal terminate (apart from kill)
* Responds to events in its watched dir, apart from to build file itself

Build file writing:

* Single ninjasub
  * Alone in file
  * At start of file
  * At end
  * In middle
* Multiple
  * At start of file
  * At end
  * In middle
* Check carefully that we do right thing with newlines everywhere
* Don't change build file if no need
* Test indentation
* Ninjasub directive itself
  * A file extension anywhere in the repo `*.md`
  * File(s) in a specific dir of the repo

## Roadmap

* Specify a build file other than build.ninja.
* Configure the timeout
* Daemon to cope with kill signal

## Contributing

It's great that you're interested in contributing. Please ask questions by
raising an issue and PRs will be considered. For full details see
[CONTRIBUTING.md](CONTRIBUTING.md)

## License

We declare our licensing by following the REUSE specification - copies of
applicable licenses are stored in the LICENSES directory. Here is a summary:

* Source code is licensed under GPL-3.0-or-later.
* Anything else that is not executable, including the text when extracted from
  code, is licensed under CC-BY-SA-4.0.
* Where we use a range of copyright years it is inclusive and shorthand for
  listing each year individually as a copyrightable year in its own right.

For more accurate information, check individual files.

Ninjasub is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.
